const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({

	firstName: {
		type: String,
		required:[true, 'Full name is required']
	},
	lastName: {
		type: String,
		required:[true, 'Full name is required']
	},
	mobileNo: {
		type: String,
		required:[true, 'Mobile number is required']
	},
	email: {
		type: String,
		required:[true, 'Email address is required']
	},
	password: {
		type: String,
		required:[true, 'Password is required']
	},
	isAdmin: {
		type: Boolean,
		default: false
	}
	,
	orders: [{
		products: [{
			productId: {
				type: String,
				required:[true, 'Product Id is required']
			},
			productName: {
				type: String,
				required:[true, 'Product name is required']
			},
			quantity: {
				type: Number,
				required:[true, 'Quantity is required']
			}
		}],
		totalAmount: {
				type: Number,
				required:[true, 'Total amount is required']
			},
		purchasedOn: {
				type: Date,
				default: new Date()
			}

	}]

	})

module.exports = mongoose.model('User', userSchema)