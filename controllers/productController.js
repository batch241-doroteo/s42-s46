const Product = require('../models/Product')

// SESSION 43
// Create Product for Admin WORKING!!!

module.exports.addProduct = (reqBody) => {


	let productData = new Product({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	});

	return productData.save().then((product, error) => {

		if(error) {
			return false
		} else {
			return true
		}
	});

};

// Retrieve all active products WORKING!!!

module.exports.getActiveProduct = () => {

	return Product.find({isActive: true}).then(result => {
		return result;
	})

}
// SESSION 44

// Retrive Single Product WORKING!!!

module.exports.getSingleProduct = (reqParams) => {

	return Product.findById(reqParams.productId).then(result => {
		return result;
	})

}

// Update Product PENDING!


module.exports.updateProduct = (isAdmin, reqParams, reqBody) => {
    if (isAdmin) {
        const updateProduct = {
            name: reqBody.name,
            description: reqBody.description,
            price: reqBody.price,
            isActive: reqBody.isActive,
        };

        return Product.findByIdAndUpdate(reqParams.productId, updateProduct, {
            new: true,
        })
            .then((product) => {
                return true;
            })
            .catch((error) => {
                return false;
            });
    } else {
        const message = "User must be ADMIN to access this.";
        return Promise.resolve({ value: message });
    }
};




///////////////////////////////////////////////////////

// Archive Product

module.exports.archiveProduct = (data, reqParams, reqBody) => {

	if(data.isAdmin) {

	let archiveProduct = {
		isActive: reqBody.isActive
	}

		/*Syntax:
		findByIdandUpdate(document ID, updatetobeapplied) */

	return Product.findByIdAndUpdate(reqParams.productId, archiveProduct).then((product, error) => {
		if(error) {
			return false
		} else {
			return true
		}
	})
};
	let message = Promise.resolve('User must be ADMIN to access this.')
	return message.then((value) => {
		return {value};
	})
}

// FIND ALL PRODUCTS (ADMIN ACCESS)

module.exports.getAllProduct = () => {

	return Product.find({}).then(result => {
		return result;
	})

}