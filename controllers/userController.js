const User = require('../models/User');
const Product = require('../models/Product');
const bcrypt = require('bcrypt');
const auth = require('../auth')

// Checking if the email exists in the database.
module.exports.checkEmailExists = (reqBody) => {
	return User.find({email:reqBody.email}).then(result => {

		if(result.length > 0) {
			return true

		// no duplicate email found // User is not yet registered in our database
		} else {
			return false
		}
	})
}

// User Registration

module.exports.registerUser = (reqBody) => {

	let newUser = new User({

		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		mobileNo: reqBody.mobileNo,
		email: reqBody.email,
		// bcrypt.hashSync(<dataToBeHash>, <saltRound>)
		password: bcrypt.hashSync(reqBody.password, 10)
	});

	return newUser.save().then((user, error) => {

		if(error) {
			return false
		} else {
			return true
		}
	});
};



// User Authentication

module.exports.loginUser = (reqBody) => {

	return User.findOne({email: reqBody.email}).then(result => {

		if(result == null) {
			return false
		} else {

			// compareSync(dataFromRequestBody, encryptedDataFromDatabase) 
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if(isPasswordCorrect) {
				return {access: auth.createAccessToken(result)}
			} else {
				return false
			}
		}
	})
}

// Retreive user details (For Frontend)
module.exports.getProfile = (userData) => {
	return User.findById(userData.id).then(result => {
		// console.log(data.userId);
		// console.log(result);
		
		if (result == null) {
			return false
		} else {
			result.password = "*****"

			// Returns the user information with the password as an empty string or asterisk.
			return result
		}
	})
};

// Retrieve User Details

module.exports.getOneUser = (userId, reqBody) => {

	return User.findById(userId).then(result => {
		return result;
	})
}

// Set user as admin (admin only) // STRETCH GOAL

module.exports.setAdmin = (data, reqParams, reqBody) => {

	if(data.isAdmin) {
		let newAdmin = {
			isAdmin: reqBody.isAdmin
		};

		/*
	SYNTAX:
	findByIdAndUpdate(document ID, updatesToBeApplied)
	*/

		return User.findByIdAndUpdate(reqParams.userId, newAdmin).then((user, error) => {
			if(error) {
				return false
			} else {
				return true
			}
		});

	};
	let message = Promise.reject('User must be ADMIN to set a new admin.')
	return message.then((message) => {
		return {message};
	})
};

// CREATE ORDER


module.exports.createOrder = async (data) => {

	let totalPrice;
	let nameOfProduct;

	let isProductUpdated = Product.findById(data.productId).then(result => {
		totalPrice = result.price * data.quantity
		nameOfProduct = result.name
	});


	let isUserUpdated = await User.findById(data.userId).then(user => {

		// Adds the productId in the user's products array

		user.orders.push({
			products: [{
				productId: data.productId,
				productName: nameOfProduct,
				quantity: data.quantity
			}],
			totalAmount: totalPrice
		});


		return user.save().then((user, error) => {
			if (error){
				return false;
			} else {
				return true;
			};
		});
	});

	if(isUserUpdated && isProductUpdated){
			// Order successful
		return true;
	} else {
			// failed order
		return false;
	};

}; //end of module

// RETRIEVE AUTHENTICATED USER'S ORDER // STRETCH GOAL

module.exports.userOrders = (reqBody) => {

	return User.findOne({email: reqBody.email}).then(result => {

		if(result == null) {
			return false
		} else {

			// compareSync(dataFromRequestBody, encryptedDataFromDatabase) 
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if(isPasswordCorrect) {
				return ({orders: reqBody.orders, result})
			}
			else {
				return false
			}
		}
	})
}

// RETRIVE ALL ORDERS (ADMIN ACCESS)

// module.exports.allOrders = (reqBody) => {

// if(data.isAdmin) {
// 	return User.find().then(result => {
// 		return result;
// 	})
// }; // end of if statement

// let message = Promise.resolve('Only Admin can access Orders')
// 	return message.then((value) => {
// 		return {value};
// 	})

// }












