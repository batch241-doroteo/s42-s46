const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController')
const auth = require('../auth')

///////////////////// Session 42

// CHECK EMAIL IF EXISTING

router.post('/checkEmail', (req, res) => {

	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController))
})

// User Registration

router.post('/register', (req, res) => {

	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
})

// User Authentication

router.post('/login', (req, res) => {

	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
})

// Retreive user details (For Frontend)
router.get("/details", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
		console.log(userData)
		console.log(req.headers.authorization);
	userController.getProfile({id: userData.id}).then(resultFromController => res.send(resultFromController))
});

// Retrieve User Details

router.get('/:id', (req, res) => {

	userController.getOneUser(req.params.id).then(resultFromController => res.send(resultFromController));
});


// SET USER AS ADMIN (ADMIN ACCESS ONLY) // STRETCH GOAL

router.put("/:userId/newAdmin", auth.verify, (req, res) => {

	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	userController.setAdmin(data, req.params, req. body).then(resultFromController => res.send(resultFromController))
});

// CREATING ORDER (NON ADMIN USERS)

router.post('/createOrder', auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)

	if(userData.isAdmin == true) 
	{
		res.send(false)
	} 
	else {
	let data =  {
		userId: auth.decode(req.headers.authorization).id,
		productId: req.body.productId,
		quantity: req.body.quantity
	}

		userController.createOrder(data).then(resultFromController => res.send(resultFromController))
	
	console.log(data)	
	} // end of else statement

})

// RETRIEVE AUTHENTICATED USER'S ORDER // STRETCH GOAL

router.post('/userOrders', (req, res) => {


	userController.userOrders(req.body).then(resultFromController => res.send(resultFromController));

});

// RETRIVE ALL ORDERS (ADMIN ACCESS)

// router.get('/allOrders', (req, res) => {

// 	const data = {
// 		isAdmin: auth.decode(req.headers.authorization).isAdmin
// 	}

// 	userController.allOrders().then(resultFromController => res.send(resultFromController));
// });











module.exports = router;