	const express = require('express');
const router = express.Router();
const productController = require('../controllers/productController')
const auth = require('../auth')

// SESSION 43

// Create a Product with Admin Authorization WORKING!!!

router.post('/newProduct', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)

	if(userData.isAdmin == false) {
		res.send(false)
	} else {
		productController.addProduct(req.body).then(resultFromController => res.send(resultFromController))
	}
	

})

// Retrieve all active products WORKING!!!

router.get('/activeProduct', (req, res) => {
	productController.getActiveProduct(req.body).then(resultFromController => res.send(resultFromController))
})

// SESSION 44

// Retrive Single Product WORKING!!!

router.get('/:productId/getProduct', (req, res) => {
	productController.getSingleProduct(req.params).then(resultFromController => res.send(resultFromController))
})

// Updating Product / Admin only WORKING


router.put("/:productId/update", auth.verify, (req, res) => {
    // const data = auth.decode(req.headers.authorization).isAdmin;

    const { isAdmin } = auth.decode(req.headers.authorization);

    productController
        .updateProduct(isAdmin, req.params, req.body)
        .then((resultFromController) => res.send(resultFromController));
});


////////////////////////////////////////////////////

// Archive Product / Admin only WORKING

router.patch('/:productId/archive', auth.verify, (req, res) => {

	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	productController.archiveProduct(data, req.params, req.body ).then(resultFromController => res.send(resultFromController))
})

// FIND ALL PRODUCTS (ADMIN ACCESS)

router.get('/allProduct', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)

	if(userData.isAdmin == false) {
		return res.send("Sorry but you don't have access to this.")
	} else {
		productController.getAllProduct(req.body).then(resultFromController => res.send(resultFromController))
	}
	

})





module.exports = router;